<?php

declare(strict_types=1);

namespace App\Service;

use Exception;
use PDO;
use App\Service\GoogleApiService;

class UserService {

    /** @var PDO */
    private $dbConnection;

    /** @var GoogleApiService */
    private $googleApiService;

    /**
     * @param PDO $dbConnection
     */
    public function __construct(PDO $dbConnection, GoogleApiService $googleApiService) {
        $this->dbConnection = $dbConnection;
        $this->googleApiService = $googleApiService;
    }

    /**
     * @param array $params
     *
     * @return array
     */
    public function get(array $params): array {
        $query = "select id, first_name as firstName, last_name as lastName, email, phone, ip, link, comments, creation_date as creationDate from users where true ";
        if (!empty($params["search"])) {
            $search = strtolower($params["search"]);
            $query .= " and (lower(first_name) like '%$search%' or lower(last_name) like '%$search%') ";
        }
        if (!empty($params["sort"])) {
            $query .= " order by {$params["sort"]} ";
            if (!empty($params["order"])) {
                $query .= " {$params["order"]}";
            }
        }
        if (!empty($params["offset"]) || !empty($params["limit"])) {
            $query .= " limit";
            if (array_key_exists("offset", $params)) {
                $query .= " {$params["offset"]} ";
            }
            if (!empty($params["limit"])) {
                $query .= ", {$params["limit"]} ";
            }
        }

        $users = $this->dbConnection->query($query)->fetchAll(PDO::FETCH_ASSOC);

        foreach ($users as &$user) {
            $user["actions"] = "
                <div class='row'>
                    <div class='btn btn-warning update-user-btn' title='Update' 
                            data-id='{$user["id"]}' 
                            data-first-name='{$user["firstName"]}' 
                            data-last-name='{$user["lastName"]}'
                            data-email='{$user["email"]}' 
                            data-phone='{$user["phone"]}'
                            data-ip='{$user["ip"]}' 
                            data-link='{$user["link"]}'
                            data-comments='{$user["comments"]}' 
                            data-creation-date='{$user["creationDate"]}'
                            ><i class='fa fa-edit'></i></div>
                    <div class='btn btn-danger delete-user-btn' title='Delete' data-id='{$user["id"]}'><i class='fa fa-trash'></i></div>
                </div>";
        }

        $query = "select count(id) from users";
        $count = $this->dbConnection->query($query)->fetchColumn();

        return [
            "rows" => $users,
            "total" => $count
        ];
    }

    /**
     * @param array $params
     *
     * @throws Exception
     *
     * @return void
     */
    public function save(array $params): void {
        if (empty($params["firstName"])) {
            throw new Exception("First name is missing");
        }
        if (empty($params["lastName"])) {
            throw new Exception("Last name is missing");
        }
        if (!empty($params["id"])) {
            if (empty($this->dbConnection->query("select id from users where id = {$params["id"]}")->fetchColumn())) {
                throw new Exception("Unknown User with ID - {$params["id"]}");
            }
        }

        if (!empty($params['id'])) {
            $query = "update users set 
                                    first_name = '{$params['firstName']}', 
                                    last_name = '{$params['lastName']}',
                                    email = '{$params['email']}', 
                                    phone = '{$params['phone']}',
                                    ip = '{$params['ip']}', 
                                    link = '{$params['link']}',
                                    comments = '{$params['comments']}', 
                                    updated_date = now()
                        where id={$params['id']}";
        } else {
            $query = "insert into users (first_name, last_name, email, phone, ip, link, comments, creation_date, updated_date) values 
                                        ('{$params["firstName"]}', '{$params["lastName"]}', '{$params["email"]}', '{$params["phone"]}',
                                         '{$params["ip"]}', '{$params["link"]}', '{$params["comments"]}', now(), now())";
        }

        $this->dbConnection->query($query);

        $this->googleApiService->migrate();
    }

    /**
     * @param int $id
     *
     * @throws Exception
     *
     * @return void
     */
    public function delete(int $id): void {
        if (empty($this->dbConnection->query("select id from users where id = $id")->fetchColumn())) {
            throw new Exception("Unknown User with ID - $id");
        }

        $this->dbConnection->query("delete from users where id = $id");
        $this->googleApiService->migrate();
    }
}