<?php

declare(strict_types=1);

namespace App\Service;

use PDO;

class MigrateService {

    /** @var PDO */
    private $dbConnection;

    /**
     * @param PDO $dbConnection
     */
    public function __construct(PDO $dbConnection) {
        $this->dbConnection = $dbConnection;
    }

    /**
     * @return void
     */
    public function execute(): void {
        $statements = [
                'CREATE TABLE if not exists users (
                        `id` int NOT NULL AUTO_INCREMENT,
                        `last_name` varchar(255),
                        `first_name` varchar(255),
                        `email` varchar(255),
                        `phone` varchar(255),
                        `ip` varchar(255),
                        `link` TEXT,
                        `comments` TEXT,
                        `creation_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                        `updated_date` TIMESTAMP,
                        PRIMARY KEY (`id`)
                )',
                'CREATE TABLE if not exists countries (
                            `id` int NOT NULL AUTO_INCREMENT,
                            `name` varchar(255),
                            `iso` varchar(255),
                            PRIMARY KEY (`id`)
            )',
            'CREATE TABLE if not exists languages     (
                            `id` int NOT NULL AUTO_INCREMENT,
                            `name` varchar(255),
                            `family` varchar(255),
                            PRIMARY KEY (`id`)
            )'
        ];
        foreach ($statements as $statement) {
            $this->dbConnection->query($statement);
        }

        $query = "insert into users (first_name, last_name, email, phone, ip, link, comments) values
                                        ('Anna', 'Aramyan', 'anna.aramyan@gmail.com', '+37494555551',
                                         '78.109.77.146 ', 'https://developers.google.com/sheets/api/samples/writing',
                                         'Some comments for Anna'),
                                          ('Armen', 'Hakobyan', 'armen.hakobyan@gmail.com', '+37494578551',
                                         '78.109.77.125 ', 'https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets.values/clear',
                                         'Some comments for Armen'),
                                          ('Artur', 'Karapetyan', 'artur.karapetyan@gmail.com', '+37494572551',
                                         '78.109.77.789 ', 'https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets.values/batchGet',
                                         'Some comments for Artur')";
        $this->dbConnection->query($query);

        $query = "insert into countries (name, iso) values
                                        ('Armenia', 'am'), ('Germany', 'de'),  ('Belgium', 'be'), ('Denmark', 'dk'), ('Greece', 'gr')";
        $this->dbConnection->query($query);

        $query = "insert into languages (name, family) values
                                        ('Mandarin Chinese', 'Sino-Tibetan'), ('Spanish', 'Indo-European'),
                                            ('Japanese', 'Japonic'), ('Tunisian Arabic', '	Afroasiatic')";
        $this->dbConnection->query($query);
    }
}