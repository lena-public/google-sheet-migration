<?php

declare(strict_types=1);

namespace App\Service;

use Google_Client;
use Google_Service_Sheets;
use Google_Service_Sheets_ClearValuesRequest;
use Google_Service_Sheets_ValueRange;
use Google\Exception as GoogleException;
use PDO;

class GoogleApiService {

    /** @var PDO */
    private $dbConnection;

    /** @var array */
    private $config;

    /** @var Google_Client */
    private $client;

    /** @var Google_Service_Sheets */
    private $googleSheetService;

    /**
     * @param PDO   $dbConnection
     * @param array $config
     *
     * @throws GoogleException
     */
    public function __construct(PDO $dbConnection, array $config) {
        $this->dbConnection = $dbConnection;

        $this->config = $config;

        $this->client = new Google_Client();
        $this->client->setAuthConfig('../credentials.json');
        $this->client->addScope("https://www.googleapis.com/auth/spreadsheets");

        $this->googleSheetService = new Google_Service_Sheets($this->client);
    }

    /**
     * @return void
     */
    public function migrate(): void {
        foreach ($this->config["spread_sheet_files"] as $spreadSheetFile) {
            $spreadsheetId = $spreadSheetFile["id"];

            foreach ($spreadSheetFile["sheets"] as $sheet) {
                $range = $sheet["name"];
                $requestBody = new Google_Service_Sheets_ClearValuesRequest();
                $this->googleSheetService->spreadsheets_values->clear($spreadsheetId, $range, $requestBody);

                $rowNumber = 1;
                foreach ($sheet["tables"] as $table) {
                    $range = $sheet["name"] . "!A" . $rowNumber;
                    $query = "select `" . implode("`,`", $table["columns"]) . "` from `{$table["name"]}`";
                    $data = $this->dbConnection->query($query)->fetchAll(PDO::FETCH_ASSOC);

                    $values = [
                        [$table["name"]],
                        $table["columns"]
                    ];
                    foreach ($data as $row) {
                        $values[] = array_values($row);
                    }

                    $requestBody = new Google_Service_Sheets_ValueRange();
                    $requestBody->setValues($values);
                    $params = ["valueInputOption" => "RAW"];
                    $this->googleSheetService->spreadsheets_values->update($spreadsheetId, $range, $requestBody, $params);

                    $rowNumber += count($data) + 3;
                }
            }
        }
    }
}