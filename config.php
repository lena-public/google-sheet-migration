<?php

declare(strict_types=1);

return [
    "db" => [
        "host"     => "localhost",
        "user"     => "root",
        "password" => "root12",
        "dbName"   => "googlesheet"
    ],
    "google_api" => [
        "spread_sheet_files" => [
            [
                "id" => "1pw4PlrcmP2S3cHI2X7GQLNitr2_SmENQGl3JxeQBaTI",
                "sheets" => [
                    [
                        "name" => "Sheet1",
                        "tables" => [
                            [
                                "name" => "users",
                                "columns" => ["id", "first_name", "last_name", "email", "phone", "ip", "link", "comments", "creation_date", "updated_date"]
                            ],
                            [
                                "name" => "languages",
                                "columns" => ["id", "name"]
                            ],
                            [
                                "name" => "countries",
                                "columns" => ["id", "name", "iso"]
                            ]
                        ]
                    ],
                    [
                        "name" => "Sheet2",
                        "tables" => [
                            [
                                "name" => "languages",
                                "columns" => ["id", "name", "family"]
                            ],
                            [
                                "name" => "users",
                                "columns" => ["id", "first_name", "creation_date", "updated_date"]
                            ]
                        ]
                    ]
                ]
            ],
            [
                "id" => "1iOeRGicoI6bAYzUoeKvSvTsud9Hm86llHTIeT6PapP8",
                "sheets" => [
                    [
                        "name" => "lena",
                        "tables" => [
                            [
                                "name" => "users",
                                "columns" => ["id", "first_name", "last_name", "email", "phone", "ip", "link", "comments", "creation_date", "updated_date"]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ]
];