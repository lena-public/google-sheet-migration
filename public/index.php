<?php

declare(strict_types=1);

require_once '../vendor/autoload.php';
$config = require_once '../config.php';

use App\Service\UserService;
use App\Service\MigrateService;
use App\Service\GoogleApiService;

$dbConnection = new PDO("mysql:host={$config["db"]["host"]};dbname={$config["db"]["dbName"]}", $config["db"]["user"], $config["db"]["password"]);
$googleApiService = new GoogleApiService($dbConnection, $config['google_api']);

if (array_key_exists("get-user-list", $_REQUEST)) {
    $userService = new UserService($dbConnection, $googleApiService);

    echo json_encode($userService->get($_REQUEST));
    die;
}
if (array_key_exists("save-user", $_REQUEST)) {
    $userService = new UserService($dbConnection, $googleApiService);

    try {
        $userService->save($_REQUEST);

        $response = ["success" => true];
    } catch (Exception $e) {
        $response = ["success" => false, "errorMessage" => $e->getMessage()];
    }

    echo json_encode($response);
    die;
}
if (array_key_exists("delete-user", $_REQUEST)) {
    $userService = new UserService($dbConnection, $googleApiService);

    try {
        $userService->delete(intval($_REQUEST["id"]));

        $response = ["success" => true];
    } catch (Exception $e) {
        $response = ["success" => false, "errorMessage" => $e->getMessage()];
    }

    echo json_encode($response);
    die;
}
if (array_key_exists("migrate-db", $_REQUEST)) {
    $migrateService = new MigrateService($dbConnection);

    try {
        $migrateService->execute();

        $response = ["success" => true];
    } catch (Exception $e) {
        $response = ["success" => false, "errorMessage" => $e->getMessage()];
    }

    echo json_encode($response);
    die;
}
if (array_key_exists("migrate-sheet", $_REQUEST)) {
    try {
        $googleApiService->migrate();

        $response = ["success" => true];
    } catch (Exception $e) {
        $response = ["success" => false, "errorMessage" => $e->getMessage()];
    }

    echo json_encode($response);
    die;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Google Sheet Data Migration</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.20.2/dist/bootstrap-table.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <style>
        .hidden { display: none !important; }

        .loading-overlay {
            position: absolute;
            width: 100%;
            height: 100%;
            z-index: 10000;
            background: black;
            opacity: 50%;
        }

        .loading-text {
            color: white;
            font-weight: bolder;
            font-size: 60px;
            margin-top: 250px;
        }

        .grid-container { width: 100%; }
    </style>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js" integrity="sha384-VHvPCCyXqtD5DqJeNxl2dtTyhF78xXNXdkwX1CZeRusQfRKp+tA7hAShOK/B/fQ2" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/bootstrap-table@1.20.2/dist/bootstrap-table.min.js"></script>
</head>
<body>
    <div class="btn-group" role="group">
        <button type="button" class="btn btn-danger btn-migrate-db">Migrate db</button>
    </div>
    <div class="btn-group" role="group">
        <button type="button" class="btn btn-success add-user-btn">Add user</button>
    </div>
    <div class="btn-group" role="group">
        <button type="button" class="btn btn-primary btn-migrate-sheet">Export google sheet</button>
    </div>
    <div class="grid-container user-grid-container">
        <div class="row">
            <table id="user-grid"></table>
        </div>
    </div>
    <div class="form-container add-user-form hidden">
        <form>
            <input type="hidden" name="id">
            <label>First Name <input type="text" name="firstName"></label>
            <label>Last Name <input type="text" name="lastName"></label>
            <label>Email <input type="text" name="email"></label>
            <label>Phone <input type="text" name="phone"></label>
            <label>IP <input type="text" name="ip"></label>
            <label>Link <input type="text" name="link"></label>
            <label>Comments <input type="text" name="comments"></label>

            <div class="btn btn-primary save-user-btn">Save</div>
            <div class="btn btn-secondary back-to-listing-btn">Back</div>
        </form>
    </div>
</body>
    <script>
        $(function() {
            let $gridContainer = $(`.user-grid-container`),
                $formContainer = $(`.add-user-form`),
                $grid = $gridContainer.find(`#user-grid`);

            $(`.add-user-btn`).on(`click`, function () {
                $(`input[name="id"]`).val('');
                $(`input[name="firstName"]`).val('');
                $(`input[name="lastName"]`).val('');
                $(`input[name="email"]`).val('');
                $(`input[name="phone"]`).val('');
                $(`input[name="ip"]`).val('');
                $(`input[name="link"]`).val('');
                $(`input[name="comments"]`).val('');
                $(`input[name="creationDate"]`).val('');
                $gridContainer.addClass(`hidden`);
                $formContainer.removeClass(`hidden`);
            })
            $(`.back-to-listing-btn`).on(`click`, function () {
                $gridContainer.removeClass(`hidden`);
                $formContainer.addClass(`hidden`);
            });
            $(".save-user-btn").off(`click`).on(`click`, function () {
                let data = {
                    firstName: $(`input[name="firstName"]`).val(),
                    lastName: $(`input[name="lastName"]`).val(),
                    email: $(`input[name="email"]`).val(),
                    phone: $(`input[name="phone"]`).val(),
                    ip: $(`input[name="ip"]`).val(),
                    link: $(`input[name="link"]`).val(),
                    comments: $(`input[name="comments"]`).val(),
                    creationDate: $(`input[name="creationDate"]`).val()
                }
                let id = $(`input[name="id"]`).val();
                if (id) {
                    data.id = id;
                }

                showLoadingOverlay();

                $.ajax({
                    method: `post`,
                    url: `?save-user`,
                    data: data,
                    dataType: `json`,
                    success: function (response) {
                        hideLoadingOverlay();

                        if (!response.success) {
                            alert(response.errorMessage);
                        } else {
                            $grid.bootstrapTable(`refresh`);
                            $gridContainer.removeClass(`hidden`);
                            $formContainer.addClass(`hidden`);
                            alert(`Saved`);
                        }
                    }
                });
            });

            $grid.bootstrapTable({
                url: `?get-user-list`,
                sidePagination: `server`,
                pagination: true,
                columns: [
                    { field: `firstName`, title: `First Name`, align: `center`, valign: `middle`, sortable: true },
                    { field: `lastName`, title: `Last Name`, align: `center`, valign: `middle`, sortable: true },
                    { field: `email`, title: `Email`, align: `center`, valign: `middle`, sortable: true },
                    { field: `phone`, title: `Phone`, align: `center`, valign: `middle`, sortable: true },
                    { field: `ip`, title: `IP`, align: `center`, valign: `middle`, sortable: true },
                    { field: `link`, title: `Link`, align: `center`, valign: `middle`, sortable: true },
                    { field: `comments`, title: `Comments`, align: `center`, valign: `middle`, sortable: true },
                    { field: `creationDate`, title: `Creation date`, align: `center`, valign: `middle`, sortable: true },
                    { field: `actions`, align: `center`, valign: `middle`, sortable: false }
                ],
                onLoadSuccess: function () {
                    $(".update-user-btn").on(`click`, function (event) {
                        event.preventDefault();

                        let $currentTarget = $(event.currentTarget);

                        $gridContainer.addClass(`hidden`);
                        $formContainer.removeClass(`hidden`);

                        $formContainer.find(`input[name="id"]`).val($currentTarget.data(`id`));
                        $formContainer.find(`input[name="firstName"]`).val($currentTarget.data(`first-name`));
                        $formContainer.find(`input[name="lastName"]`).val($currentTarget.data(`last-name`));
                        $formContainer.find(`input[name="email"]`).val($currentTarget.data(`email`));
                        $formContainer.find(`input[name="phone"]`).val($currentTarget.data(`phone`));
                        $formContainer.find(`input[name="ip"]`).val($currentTarget.data(`ip`));
                        $formContainer.find(`input[name="link"]`).val($currentTarget.data(`link`));
                        $formContainer.find(`input[name="comments"]`).val($currentTarget.data(`comments`));
                        $formContainer.find(`input[name="creationDate"]`).val($currentTarget.data(`creation-date`));
                    });
                    $(".delete-user-btn").on(`click`, function (event) {
                        event.preventDefault();

                        let $currentTarget = $(event.currentTarget);

                        showLoadingOverlay();

                        $.ajax({
                            method: `post`,
                            url: `?delete-user`,
                            data: { id: $currentTarget.data(`id`) },
                            dataType: `json`,
                            success: function (response) {
                                hideLoadingOverlay();

                                if (!response.success) {
                                    alert(response.errorMessage);
                                } else {
                                    $grid.bootstrapTable(`refresh`);
                                    alert(`Deleted`);
                                }
                            }
                        });
                    });
                }
            });
        });

        $('.btn-migrate-db').click(function() {
            showLoadingOverlay();
            $.ajax({
                method: `post`,
                url: `?migrate-db`,
                dataType: 'json',
                success: function(response) {
                    hideLoadingOverlay();
                    if (!response.success) {
                        alert(response.errorMessage);
                    } else {
                        alert(`DB Migrated`);
                    }
                }
            });
        });

        $('.btn-migrate-sheet').click(function() {
            showLoadingOverlay();
            $.ajax({
                method: `post`,
                url: `?migrate-sheet`,
                dataType: 'json',
                success: function(response) {
                    hideLoadingOverlay();
                    if (!response.success) {
                        alert(response.errorMessage);
                    } else {
                        alert("Migrated");
                    }
                }
            });
        });

        function showLoadingOverlay () {
            $(`body`).prepend(`<div class="loading-overlay">
<div class="row">
<div class="col-md-5"></div>
<div class="col-md-2 loading-text">Loading ...</div>
<div class="col-md-5"></div>
</div>
</div>`);
        }
        function hideLoadingOverlay () {
            $(`.loading-overlay`).remove();
        }
    </script>
</html>
