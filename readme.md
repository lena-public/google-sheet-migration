### Installation

<div id="top"></div>

_Below is an example of how you can instruct your audience on installing and setting up your app. This template doesn't rely on any external dependencies or services._

1. Clone the repo
   ```sh
   https://gitlab.com/lena-public/google-sheet-migration.git
   ```
2. Configure server
   ```sh
   server {
      listen 80;
      listen [::]:80;

      server_name your_domain;

      root your_project_dir/public;
      index index.php index.html index.htm;

      location ~ \.php$ {
      include snippets/fastcgi-php.conf;

           fastcgi_pass unix:/run/php/php7.4-fpm.sock;
      }

      error_page 404 /index.php;

      location ~ /\. {
      deny all;
      }
    }
   ```
3. Create your Google access credentials
   ```sh
   https://developers.google.com/workspace/guides/create-credentials
   ```
    ```sh
    rename your config file to credentials.json
   ```
    ```sh
   paste in your project root dir
   ```
4. Configure config.php
   ```sh
     in https://prnt.sc/85yZ_0nnW-cN set https://prnt.sc/aCGIKEpZdSqU
   ```
    ```sh
    in https://prnt.sc/ozM1pQnHOxfS set https://prnt.sc/fzrGM6WeB7LR
   ```
    ```sh
   chek you file read permissions https://prnt.sc/F1a6n9A-DhSx , https://prnt.sc/-DUOaP6jy_sn
   ```
5. Open in browser, click on Migrate db (https://prnt.sc/JSdgLWw1laoB)
6. Configure cron in server
    ```sh
       * * * * * wget http://your_domain/?migrate-sheet >> /dev/null 2>&1
      ```
<p align="right">(<a href="#top">back to top</a>)</p>